from setuptools import find_packages, setup

setup(
    name='picoform',
    version='1.0.0',
    description='Very small, function based, validation library',
    packages=find_packages(),
    extras_require={
        'dev': [
            'pytest',
        ]
    },
)
