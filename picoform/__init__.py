import inspect
import json as _json
import re
import string
from datetime import date as _date, datetime as _datetime, timezone as _timezone

from functools import wraps


def utc_from_timestamp(timestamp):
    return _datetime.fromtimestamp(timestamp, _timezone.utc)


def utc_now():
    return _datetime.now(_timezone.utc)


class ValidationError(ValueError):
    def __init__(self, message):
        self.message = message


class StopValidation(Exception):
    """ Used by validators to stop the validation chain """

    def __init__(self, value):
        self.value = value


class Missing(object):
    """ Value to represent a missing value in a form """


class Field(object):
    def __init__(self, *validators, name=None):
        self.name = name
        self.validators = validators

    def clean(self, value):
        for validator in self.validators:
            try:
                value = validator(value)
            except StopValidation as e:
                return e.value
        return value


class Form(object):
    def __init__(self, values):
        self._valid = True
        self.errors = []

        self._fields = dict(inspect.getmembers(self.__class__, lambda x: isinstance(x, Field)))
        for name, field in self._fields.items():
            try:
                value = values.get(field.name or name, Missing)
                setattr(self, name, field.clean(value))
            except (ValidationError, ValueError, TypeError) as e:
                self.errors.append('%s: %s' % (name, e))
                self._valid = False

    def __bool__(self):
        return self._valid

    def __repr__(self):
        return '\n'.join('%s: %s' % (name, getattr(self, name)) for name in self._fields)


def inline(**fields):
    return type('_', (Form,), fields)


def _callable(f):
    """ Decorator to wrap functions that don't receive extra params, to make the api more consistent """

    @wraps(f)
    def _():
        return f

    return _


def in_range(min=None, max=None):
    def f(value):
        if (min is not None and value < min) or (max is not None and value > max):
            raise ValueError('not in (min, max) range')
        return value

    return f


def length(min=None, max=None):
    _in_range = in_range(min, max)

    def f(value):
        _in_range(len(value))
        # We delegate the exception raising to in_range
        return value

    return f


def regexp(exp, msg=None):
    exp = re.compile(exp)

    def f(value):
        if not exp.match(value):
            raise ValidationError(msg or 'Invalid value')
        return value

    return f


def strip(chars=None):
    def f(value):
        try:
            return value.strip(chars)
        except AttributeError:
            raise ValueError('expected text')

    return f


def optional(default=Missing, nullable=True):
    def f(value):
        if value is None:
            if nullable:
                raise StopValidation(value)
            else:
                raise ValidationError('null not accepted')
        if value is Missing:
            raise StopValidation(default)

        return value

    return f


def required(nullable=False):
    def f(value):
        if value is Missing or (not nullable and value is None):
            raise ValueError('is required')
        return value

    return f


def enum(e):
    def f(value):
        return e(value)

    return f


def list_of(type_):
    def f(value):
        try:
            return [type_(x) for x in value]
        except (ValueError, TypeError, AttributeError):
            raise ValidationError('not a list of %s' % type_.__name__)

    return f


def snap(mod):
    def f(number):
        h = number + mod // 2
        return h - h % mod

    return f


@_callable
def email(value):
    value = length(0, 256)(value)
    if '@' not in value:
        raise ValidationError('invalid email')
    return value


@_callable
def unix(value):
    return utc_from_timestamp(value)


@_callable
def after_now(value):
    if value < utc_now():
        raise ValidationError('time before now')
    return value


def date(fmt='%Y/%m/%d'):
    def f(s):
        try:
            d = _datetime.strptime(s, fmt)
        except ValueError:
            raise ValidationError('bad date')
        return d.date()

    return f


@_callable
def before_today(d):
    if d >= _date.today():
        raise ValidationError('day after today')
    return d


@_callable
def iban(code):
    clean = ''.join(code.split()).upper()
    if _number_iban(clean) % 97 != 1:
        raise ValidationError('invalid iban')
    return clean


_LETTERS = {ord(d): str(i) for i, d in enumerate(string.digits + string.ascii_uppercase)}


def _number_iban(iban):
    return int((iban[4:] + iban[:4]).translate(_LETTERS))


def json(form=None):
    def f(s):
        try:
            values = _json.loads(s)
        except (TypeError, ValueError):
            raise ValidationError('invalid json')

        if form is not None:
            return form(values)
        return values
    return f
