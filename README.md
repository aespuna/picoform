# picoform: Yet another form validation lib for python

## How to use it?

Forms can be created both using the `Form` class directly or the `inline` function.
Then you pass a dict and if the form is valid, you can retrieve the cleaned values.

```
import picoform as pf
form = pf.inline(
   name=pf.Field(pf.required(), str, pf.strip()),
   age=pf.Field(pf.optional(), int),
)
form = form({'name': 'John '})
# bool(form) == True
# form.name == 'John'
# form.age is pf.Missing
```

## Why?

The idea was to make something small, simple and easily extensible. It doesn't care about html. It doesn't care about the request.

In essence I just wanted a plugable validator for dict like structures.

Also, the optional, just works, because of the `Missing` value (`None` is not the same as a `Missing` parameter.


