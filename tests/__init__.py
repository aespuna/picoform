import datetime

import pytest

import picoform as f
from enum import Enum


class TestInRange:
    def test_min_max_defined(self):
        with pytest.raises(ValueError):
            f.in_range(min=0, max=100)(-1)

        with pytest.raises(ValueError):
            f.in_range(min=0, max=100)(101)

        assert f.in_range(min=0, max=100)(0) == 0
        assert f.in_range(min=0, max=100)(1) == 1
        assert f.in_range(min=0.0, max=0.5)(0.25) == 0.25

    def test_min_defined(self):
        with pytest.raises(ValueError):
            f.in_range(min=-1)(-2)

        assert f.in_range(min=10)(20) == 20

    def test_max_defined(self):
        with pytest.raises(ValueError):
            f.in_range(max=10)(20)

        assert f.in_range(max=10)(-2) == -2

    def test_none_defined(self):
        assert f.in_range()(100) == 100
        assert f.in_range()(2 ** 64) == 2 ** 64


class TestLength:
    def test_min_max_defined(self):
        with pytest.raises(ValueError):
            f.length(min=0, max=2)('123')

        with pytest.raises(ValueError):
            f.length(min=1, max=2)('')

        assert f.length(min=0, max=2)('') == ''
        assert f.length(min=0, max=2)('ab') == 'ab'
        assert f.length(min=0, max=2)('1') == '1'

    def test_min_defined(self):
        with pytest.raises(ValueError):
            f.length(min=5)('abcd')

        assert f.length(min=5)('abcde') == 'abcde'

    def test_max_defined(self):
        with pytest.raises(ValueError):
            f.length(max=3)('abcd')

        assert f.length(max=3)('abc') == 'abc'

    def test_none_defined(self):
        assert f.length()('abcdef') == 'abcdef'

    def test_unicode(self):
        assert f.length(max=2)('\u0110a') == '\u0110a'


class TestStrip:
    def test(self):
        with pytest.raises(ValueError):
            f.strip()(1)

        assert f.strip()(' ab ') == 'ab'


class TestOptional:
    def test_default(self):
        def test_default(default, value, expected):
            with pytest.raises(f.StopValidation) as e:
                f.optional(default=default)(value)
            assert e.value == expected

        test_default(1, f.Missing, 1)
        test_default(1, 15, 15)
        test_default(None, f.Missing, None)

    def test_nullable(self):
        with pytest.raises(ValueError):
            assert f.optional(nullable=False)(None)


class TestRequired:
    def test(self):
        with pytest.raises(f.ValidationError):
            assert f.required()(None)

        assert f.required()(1) == 1


class TestEnum:
    class E(Enum):
        V = 1

    def test(self):
        assert f.enum(TestEnum.E)(1) == TestEnum.E.V

        with pytest.raises(f.ValidationError):
            assert f.enum(TestEnum.E)(2)


class TestListOf:
    def test(self):
        assert f.list_of(int)([1, 2]) == [1, 2]
        assert f.list_of(int)([1, '2']) == [1, 2]
        assert f.list_of(str)(['a', 'b']) == ['a', 'b']

        with pytest.raises(ValueError):
            assert f.list_of(int)([1, 'a'])


class TestSnap:
    def test(self):
        assert f.snap(15)(5) == 0
        assert f.snap(15)(10) == 15
        assert f.snap(15)(60) == 60
        assert f.snap(15)(85) == 90
        assert f.snap(2)(1) == 2


class TestEmail:
    def test(self):
        assert f.email()('a@a.com') == 'a@a.com'
        assert f.email()('"a@a"@123.com') == '"a@a"@123.com'
        assert f.email()('user@10.10.10.10') == 'user@10.10.10.10'

        with pytest.raises(ValueError):
            assert f.email()('@')

        with pytest.raises(ValueError):
            assert f.email()('a@')

        with pytest.raises(ValueError):
            assert f.email()('@b')


class TestUnix:
    def test(self):
        assert f.unix()(946598400) == f.utc_from_timestamp(946598400)
        assert f.unix()(946598400.0) == f.utc_from_timestamp(946598400.0)


class TestAfterNow:
    def test(self):
        ts1 = datetime.datetime(1999, 12, 31, 16, 00, tzinfo=datetime.timezone.utc)
        ts2 = datetime.datetime(1999, 12, 31, 17, 00, tzinfo=datetime.timezone.utc)

        assert f.after_now(lambda: ts1)(ts2)

        with pytest.raises(ValueError):
            assert f.after_now(lambda: ts2)(ts1)


class TestDate:
    def test(self):
        assert f.date()('1999/12/31') == datetime.datetime(1999, 12, 31).date()
        assert f.date('%d-%Y-%m')('31-1999-12') == datetime.datetime(1999, 12, 31).date()

        with pytest.raises(ValueError):
            assert f.date()('1-2-A')
